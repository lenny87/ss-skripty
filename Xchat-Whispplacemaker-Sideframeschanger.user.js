﻿// ==UserScript==
// @Originalname Xchat-Whispplacemaker-Sideframeschanger.user.js
// @Author gooryk
// @include http://*xchat.centrum.cz/*/modchat?op=infopage*
// @exclude http://*xchat.centrum.cz/~guest~/modchat*
// ==/UserScript==


//***************************************GLOBALNI PROMENNE**************************************************************************
//**************Volitelne promenne********************
var nechciICQ_Smile=true;				//Pokud chci smajly (vpravodole) !natrvalo! ...prepisu true na false a TOTO funguje jen pokud je vse na classic verzi
var nechciTubkoStav=false;				//Pokud natrvalo nechci prepinac a zobrazovani Youtubeprehravace prepisu false na true

var nechciRadio=true;					//Pokud natrvalo nechci napravo dole Prehravac pro prehravani radia Waper (moje radio) ani jeho prepinac - prepisu false true, pozor pokud to tak chci musim mit ale pak pozapinanou Classic verzi ve funkci zmenaramu(), ostatni povypinat

//****************muj vzhled******************
//document.body.style.background="#BDE8FF url('http://xteam.jaw.cz/gooryk/admin-scripty/data/background2.jpg')";	//muj vzhled pozadi

//Refresh infookna songu probiha na jine strance (co zajistuje septani-obdelniky) - pri zmene portu a IP radia ..nutno zmenit udaje i tam !!!	
var ripadress = "84.16.224.52"
var rport = "10794"
//*****************************************************
var allframes = top.window.document.getElementsByTagName ("frameset");	//nacteni framesu hlavni stranky
var spodniramecky = allframes[2];										//frameska pro leve ramy tj sklo,infopage+frame s radkem pro text a odes. tlacitkem
var praveramecky = allframes[3];										//frameska pro prave tudiz uzivatele,ICQ,smajlci ram atd
//**********************************************************************************************************************************

//********************************************************POMOCNE FUNKCE************************************************************
function newramec(){
	var ramec=document.createElement('frame');
	ramec.id="mediaframe";ramec.name="mediaframe";ramec.scrolling="no";ramec.noResize=true;
	return ramec;
}

function zmenaramu(){			//fce co provede zmenu pravych ramu dle global. parametru a dle vyuzivatelnosti meho radia pluginu
	if (nechciRadio){
		//Classic verze	
		if (nechciICQ_Smile) praveramecky.rows="50,*,0,0,0,0,0";		//skryti prehravc a Smile ramecku
		//moje verze
		//if (nechciICQ_Smile) praveramecky.rows="50,*,0,0,0,0,160";
		//Cernajs verze
		//if (nechciICQ_Smile) praveramecky.rows="50,*,80,0,0,0,160";
		
		//Classic verze	
		else praveramecky.rows="50,*,80,0,0,0,0";
		//moje verze
		//else praveramecky.rows="50,*,80,0,0,0,160";
		//Cernajs verze
		//else praveramecky.rows="50,*,0,0,0,0,160";
	}
	else{
		//Classic verze	
		if (nechciICQ_Smile) praveramecky.rows="50,*,0,0,0,0,160";
		//moje verze
		//if (nechciICQ_Smile) praveramecky.rows="50,*,0,0,0,0,0";
		//Cernajs verze
		//if (nechciICQ_Smile) praveramecky.rows="50,*,80,0,0,0,0";
		
		//Classic verze	
		else praveramecky.rows="50,*,80,0,0,0,160";
		//moje verze
		//else praveramecky.rows="50,*,80,0,0,0,0";
		//Cernajs verze
		//else praveramecky.rows="50,*,0,0,0,0,0";
	}
	top.window.frames[3].document.getElementById("msg").focus();		//nastaveni kurzoru na radek pro psani, aby mi to neblikalo na prepinacich, vypadalo to blbe
}


function zobrazsmajly(){		//fce pro prepinac, ktera invertuje glob.parametr zobrazeni smajlu a zavola zmenu pr.ramu
	//Classic verze + moje verze
	if (nechciICQ_Smile){
	//Cernajs verze
	//if (!nechciICQ_Smile){
		nechciICQ_Smile=!nechciICQ_Smile;
		smiles.title="Pro skrytí nastavených smajlíků klikni sem.";
		stav.style.color="#00FF00";
		stav.innerHTML="&nbsp;ON&nbsp;";
	}
	else {
		nechciICQ_Smile=!nechciICQ_Smile;
		smiles.title="Pro zobrazení nastavených smajlíků klikni sem.";
		stav.style.color="#FF4D00";
		stav.innerHTML="OFF";
	}
	zmenaramu();
}

function zobrazplayer(){		//fce pro prepinac, ktera invertuje glob.parametr zobrazeni prehravace a zavola zmenu pr.ramu
	//Classic verze	
	if (nechciRadio){
	//Cernajs + moje verze
	//if (!nechciRadio){
		nechciRadio=!nechciRadio;
		player.title="Pro skrytí přehrávače klikni sem.";
		stavp.style.color="#00FF00";
		stavp.innerHTML="&nbsp;ON&nbsp;";
	}
	else {
		nechciRadio=!nechciRadio;
		player.title="Pro zobrazení přehrávače klikni sem.";
		stavp.style.color="#FF4D00";
		stavp.innerHTML="OFF";
	}
	zmenaramu();
}

function zobraztubko(){			//fce pro prepinac, ktera invertuje glob.parametr zobrazeni youtubeokna
	var glass = top.window.frames[1].frames[0];									//sklo//nejspise se tento script nekdy nacte rychleji..proto musi byt opet zde a ne v globalnich
	var tubkoplayer = glass.document.getElementById('prehraj');					//musi zde byt pro moznost OFF
	var tubkopicture = glass.document.getElementById('Ypicture');
	
	if (!nechciTubkoStav){
		var place=glass.document.getElementById('ffc');							//Proc sem? Protoze, kdyz sem to kamkoli do body stranky pripsal, prestaly fungovat JAVA okynka (nahledy obrazku uzivatelu) + neslo kliknutim na nick hodit nick do radku pro psani! Takhle nenarusuji strom prvku a tudiz vse by melo fungovat a tomuto prvku to nejak nevadi.
		place.innerHTML+="<img id='Ypicture'src='http://xteam.jaw.cz/gooryk/admin-scripty/data/youtube-logo.jpg' style='position:fixed;left:100%;top:100%;margin-top:-301px;margin-left:-401px;height:300px;width:400px'><embed id='prehraj' src='' bgcolor='#CCCCCC' wmode='transparent' type='application/x-shockwave-flash' allowfullscreen='true' allowScriptAccess='always' width='400' height='300' style='position:fixed;left:100%;top:100%;margin-top:-301px;margin-left:-401px'>";
		
		nechciTubkoStav=!nechciTubkoStav;
		youtubko.title="Pro vypnutí Youtube okna(přehrávače) klikni sem.";
		stavpy.style.color="#00FF00";
		stavpy.innerHTML="&nbsp;ON&nbsp;";
	}
	else {
		nechciTubkoStav=!nechciTubkoStav;
		youtubko.title="Pro zapnutí Youtube okna(přehrávače) klikni sem.";
		stavpy.style.color="#FF4D00";
		stavpy.innerHTML="OFF";
		tubkoplayer.parentNode.removeChild(tubkoplayer);
		tubkopicture.parentNode.removeChild(tubkopicture);
	}
	refreshskla();
}

function refreshskla(){										//fce co dokaze kliknout na odkaz obnovit
	var cil=document.getElementsByTagName("a");var a=0;					//vsechny odkazy + promenna a
	for (a=0;a<cil.length;a++)if (cil[a].innerHTML=="obnovit") break;	//vyhledani toho spravneho cile (odkaz obnovit)..casto je pokazde nekde jinde - web, mapa,... + jinak je to i v nestalych rooms
	cil = cil[a];
	var evt = document.createEvent("MouseEvents");
	evt.initEvent('click',true,true);
	cil.dispatchEvent(evt);
	top.window.frames[3].document.getElementById("msg").focus();
}

function refreshafterresize(){ 
	if (praveramecky.rows=="50,*,80,0,0,0") zmenaramu(); 	//sice by nemuselo byt, ale at se to neprovadi zbytecne pri kazdym kliku na prepinac smajliku nebo prehravace, ale jen a pouze pri samovolnem refreshi strnaky do default ramecku
}

function proved(){										 //Fce co se provede az po nacteni stranky jinak by se stalo to ze by jeste neexistovalo misto kam vlozit
//*************Zobrazeni prehravace v framesu ICQ*****************
	var mediaframe=top.window.frames[10].document.getElementsByTagName('body')[0];		//ramec pro radio
	mediaframe.style.background="#EDF1FD";
	if (!nechciRadio) mediaframe.innerHTML ="<p style='font-size:13px;margin-left:-8px;margin-top:-10px;'>&nbsp;<strong>Právě hraje:</strong><a style='position:absolute;left:100%;margin-left:-90px;color:#0055DD' href='http://radiowaper.listen2myradio.com' target='Radio_Waper' title='Klikněte pro otevření stránky s Radio Waper.'>RADIO WAPER</a><iframe id='infosong' width='188px' height='92px' src='http://"+ripadress+"/chat/fr2.php?ip="+ripadress+"&port="+rport+"&b=1&radioid=738506' onMouseOver=\"src='http://"+ripadress+"/chat/fr2.php?ip="+ripadress+"&port="+rport+"&b=1&radioid=738506'\"></iframe><br><embed id='player' src='http://radiowaper.listen2myradio.com/media/playradio738506.asx' width='336' height='50' autostart='0' type='application/x-mplayer2' pluginspage='http://www.microsoft.com/Windows/MediaPlayer/' transparentatstart='0' animationatstart='0' showcontrols='1' autosize='0' displaysize='0' showtracker='0' ShowStatusBar='1'></embed></p>";
	zmenaramu();			//zavolani funkce co provede upravu pro praveramy
	top.window.frames[6].addEventListener("resize", function () {refreshafterresize()}, false);			//nekdy se vrati do default (cca co 4min) , tak aby se to zase spravilo na nami pozadovane ramy
	proved2();				//uplne nakonec prednacteni scriptu pro vek
}
//**********************************************************************************************************************************

//********************************************************Hlavni program************************************************************
top.window.addEventListener("load", function () {proved()}, false);
spodniramecky.rows="*,55,55";							//priprava stranky (rozsieni ramu) pro septaci obdelnicky
praveramecky.appendChild(newramec());					//vlozi novy ramec pro radio

//************Zobrazeni prepinacu jak pro smajly tak pro prehravac, youtubko**************
document.body.innerHTML+="<iframe id='ageframe' style='position:absolute;visibility:hidden;width:0px;height:0px;left:0px;top:0px' src=''></iframe>";		//iframe pro prednacteni veku

kam = document.getElementsByTagName ("p")[0];			//misto kam vlozim prepinace na zobraz. a skryvani - vpravo dole
//Classic verze
if (!nechciRadio)kam.innerHTML+="<span id='player' style='position:absolute;left:100%;margin-left:-76px;top:40px;cursor:pointer;font-weight:bold' title='Pro skrytí přehrávače klikni sem.'>Přehrávač <span id='stavp' style='color:#00FF00;background-color:black;'>&nbsp;ON&nbsp;</span></span>";
//Cernajs + moje verze
//if (!nechciRadio)kam.innerHTML+="<span id='player' style='position:absolute;left:100%;margin-left:-76px;top:40px;cursor:pointer;font-weight:bold' title='Pro zobrazení přehrávače klikni sem.'>Přehrávač <span id='stavp' style='color:#FF4D00;background-color:black;'>OFF</span></span>";
//Classic verze + moje verze
if (nechciICQ_Smile)kam.innerHTML+="<span id='smiles' style='position:absolute;left:100%;margin-left:-65px;top:22px;cursor:pointer;font-weight:bold' title='Pro zobrazení nastavených smajlíků klikni sem.'>Smajlíci <span id='stav' style='color:#FF4D00;background-color:black;'>OFF</span></span>";
//Cernajs verze
//if (nechciICQ_Smile)kam.innerHTML+="<span id='smiles' style='position:absolute;left:100%;margin-left:-65px;top:22px;cursor:pointer;font-weight:bold' title='Pro skrytí nastavených smajlíků klikni sem.'>Smajlíci <span id='stav' style='color:#00FF00;background-color:black;'>&nbsp;ON&nbsp;</span></span>";
if (!nechciTubkoStav) kam.innerHTML+="<span id='youtub' style='position:absolute;left:100%;margin-left:-64px;top:5px;cursor:pointer;font-weight:bold' title='Pro zapnutí Youtube okna(přehrávače) klikni sem.'>Youtube <span id='stavpy' style='color:#FF4D00;background-color:black;'>OFF</span></span>";

var smiles = document.getElementById('smiles');
if (smiles!=null) smiles.addEventListener("click", function () {zobrazsmajly()}, false); //prirazeni funkce pro prepinac
var stav = document.getElementById('stav');
var player = document.getElementById('player');
if (player!=null) player.addEventListener("click", function () {zobrazplayer()}, false); //prirazeni funkce pro prepinac
var stavp = document.getElementById('stavp');
var youtubko = document.getElementById('youtub');
youtubko.addEventListener("click", function () {zobraztubko()}, false); 				//prirazeni funkce pro prepinac
var stavpy = document.getElementById('stavpy');
//********************************************************************************
//**********************************************************************************************************************************

//*************************************Prednacteni scriptu pro vek pro rychlejsi spousteni******************************************
function proved2(){
	//********************Malinká pomoc proti čekání načtení scriptu se zakázanými vstupy****************
	document.getElementById("ageframe").src="http://xteam.jaw.cz/gooryk/vek/data/vek.php";
}