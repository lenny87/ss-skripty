﻿// ==UserScript==
// @Originalname Xchat-Side-Bar-Appl.user.js
// @Author gooryk
// @include http://*xchat.centrum.cz/*/modchat?op=userspage*
// @exclude http://*xchat.centrum.cz/~guest~/modchat*
// ==/UserScript==

//***************************************GLOBALNI PROMENNE**************************************************************************
//**************Volitelne promenne********************
var email="";											//Zadej mezi uvozovky svuj email pokud neni jiz uveden nize
//*****************************************************
var oblibeni_divup = document.getElementById("crdiv1");
//****ORIGINALNI ziskani dat z kodu hlavni stranky..zarucuje ze bude existovat vzdy a tim konecne odstranim plno problemu - nyni novejsi verze osetreni proti zmenam nyni hleda v celem HTML kodu****					
var data = top.window.document.getElementsByTagName("html")[0].innerHTML;		//nacteni zdroje dat
var rid = data.split("var rid=")[1].split(";")[0];
var uid = data.split("var uid=")[1].split(";")[0];
var hash = data.split("var my_auth='")[1].split("';")[0];
var glass;
//**************ziskani emailu pomoci UID *******************
if (uid==11992435) email = "sara12@centrum.cz";
else if (uid==9431549) email = "*4661*mi.sha@seznam.cz";
//***********************************************************
//**********************************************************************************************************************************

//********************************************************INFO OBDELNIKY************************************************************
oblibeni_divup.innerHTML = "<iframe id='xuser' width='160px' height='90px' src=''></iframe><br>"+"<iframe id='xroom' width='160px' height='90px' src=''></iframe><br>"+"<iframe style='visibility:hidden;width:0px;height:0px' id='sounder'src=''></iframe><br>"+"<b>Zvol délku filtru: </b><select id='delkaf' title='Zvol délku filtru v minutách.' style='font-style:obligue;font-size:xx-small;font-weight:bold;border:solid blue ;color:black;background-color: white;width:72px;margin-bottom:5px'><option value='120'>120</option><option value=''>Zadat</option><option value='90'>90</option><option value='60'>60</option><option value='30'>30</option><option value='10'>10</option><option value='0'>Zrušit</option></select><br>"+"<p id='admbuttondiv' style='margin-bottom:4px'></p><span id='zobraz' style='color:blue;font-size:12px;font-weight:bold'>Smajlíci >>><br><img src='http://xteam.jaw.cz/gooryk/admin-scripty/data/Blue-arrow.png' style='position:relative;left:58px;height:50px;width:50px'><br><span style='position:relative;left:100%;margin-left:-70px'><<< Smajlíci</span><br><br></span><span id='smileybuttondiv' style='display:none'></span>"+oblibeni_divup.innerHTML;

var admbutdiv = document.getElementById("admbuttondiv");
var smbutdiv = document.getElementById("smileybuttondiv");
var zobrazsm = document.getElementById("zobraz");
var misto = document.getElementById("xuser");
var oroom = document.getElementById("xroom");
//**********************************************************************************************************************************

//***********************************************VYTVORENI ADMIN TLACITEK***********************************************************
function br1() admbutdiv.appendChild(document.createElement('br')); //vlozi zalomeni radku cili prechod na dalsi radek
function AdmButt(nazev, udalost, tooltip) admbutdiv.appendChild(create_admin_button(nazev, udalost, tooltip)); //aby se to nemuselo porad vypisovat

    AdmButt('AKCE', action, "'AKCE' - Slouží pro více akcí, pro nápovědu napište /? a klikněte sem.");
    AdmButt('KLICE', klice, "'KLICE' - Zobrazí klíčované nicky v místnosti.");
    AdmButt('NFLTR', nastavfiltr,"'NASTAV FILTR' - Levé tlačítko myši nastaví filtr v místnosti dle zvolené délky filtru, pravé je pro rychle zrušení filtru.");
    AdmButt('ZÁPIS', zapisfiltr,"'ZÁPIS' - Zápis časového filtru na fórum.");
    br1();
    AdmButt('UKALL', unkickall, "'UNKICKALL - Unkick všech vyhozených nicků.");
    AdmButt('PRFIL', profil, "'PROFIL' - Zobrazení profilu nicku.");
    AdmButt('SS', stalyspravce, "'SS' - Vyhledání nicku ve stálých správcích.");
    AdmButt('LK/UK', dvere, "'LOCK/UNLOCK' - Zamčení místnosti - levé tlačítko myši. Odemčení - pravé tlačítko.");
    br1();
//**********************************************************************************************************************************

//***********VYTVORENI SMAJLIKOVYCH TLACITEK....Zadejte cisla smajliku dle rad******************************************************
function br2() smbutdiv.appendChild(document.createElement('br')); //vlozi zalomeni radku cili prechod na dalsi radek
function SmButt(number)smbutdiv.appendChild(create_smiley_button(number)); //aby se to nemuselo porad vypisovat
//Kdo nechce tolik rad muze jednotlive rady vcetne zalomeni br2() zalomitkovat: //Smbutt(354);SmButt(......;//br2();
//Pro zmenu obsahu smajlu staci zmenit pouze cislo napr chci *3* tak prepisu nejaky na SmButt(3)
							// 1. rada
    SmButt(3937); SmButt(3328); SmButt(5335); SmButt(4602); SmButt(5031); SmButt(3725); SmButt(4025);
    br2();					// 2. rada
  	SmButt(811); SmButt(4603); SmButt(4504); SmButt(1612); SmButt(5290); SmButt(4225); SmButt(5270);
    br2();					// 3. rada
    SmButt(447); SmButt(2009); SmButt(2404); SmButt(176); SmButt(4103); SmButt(3004); SmButt(3525);
    br2();					// 4. rada
  	SmButt(2711); SmButt(2654); SmButt(3907); SmButt(5204); SmButt(4153); SmButt(2801); SmButt(2836);
    br2();					// 5. rada
  	SmButt(3500); SmButt(3624); SmButt(2464); SmButt(5600); SmButt(5456); SmButt(4161); SmButt(1341);
    br2();					// 6. rada
  	SmButt(5370); SmButt(4107); SmButt(4403); SmButt(3664); SmButt(4913); SmButt(3213); SmButt(1000);
   	br2();					// 7. rada
  	SmButt(909); SmButt(1440); SmButt(926); SmButt(317); SmButt(2760); SmButt(3103); SmButt(1318);
    br2();					// 8. rada
  	SmButt(2838); SmButt(2633); SmButt(4122); SmButt(1200); SmButt(5096); SmButt(3926); SmButt(5231);
  	br2();					// 9. rada
  	SmButt(1513); SmButt(4620); SmButt(4522); SmButt(2433); SmButt(4704); SmButt(5339); SmButt(1201);
  	br2();					// 10. rada
  	SmButt(2459); SmButt(4160); SmButt(1362); SmButt(3863); SmButt(2565); SmButt(5570); SmButt(4572);
  	br2();					// 11. rada
  	SmButt(2757); SmButt(3673); SmButt(3873); SmButt(3973); SmButt(174); SmButt(5479); SmButt(3880);
  	br2();					// 12. rada
  	SmButt(3883); SmButt(4485); SmButt(3486); SmButt(4087); SmButt(5188); SmButt(290); SmButt(2490);
  	br2();					// 13. rada
  	SmButt(3393); SmButt(5495); SmButt(2496); SmButt(2696); SmButt(2794); SmButt(255); SmButt(4152);
  	br2();					// 14. rada
  	SmButt(4721); SmButt(4740); SmButt(3602); SmButt(4440); SmButt(4157); SmButt(3805); SmButt(920);
    br2();br2();

zobrazsm.addEventListener("mouseover", function () {zobrazsmajly()}, false);	
this.addEventListener("resize", function () {oblibeni_divup.style.height = (this.innerHeight-30)+'px';}, false);	//zaruceni pri resize refresh useriframe neboli teto stranky s uzivateli atd ..abych nemel uzivatele mimo stranku//je to dulezite pro napr prepinace smiles a prehravac a autorefresh na default ramy apod
//**********************************************************************************************************************************

//********************************************************POMOCNE FUNKCE************************************************************
function zobrazsmajly(){
	smbutdiv.style.display='inline';
	zobrazsm.style.display='none';
}

function skryjsmajly(){
	smbutdiv.style.display='none';
	zobrazsm.style.display='inline';
}

function create_smiley_button(number){  										// pro vytvoreni smajlikoveho tlacitka
    var button = this.document.createElement('button');
    var mod_numb = number % 100;
    button.style.font = "7pt Arial";
    button.style.width = "21px";
    button.style.height = "15px";
    button.style.border = "1";
    button.style.background = 'url("http://img.xchat.centrum.cz/images/x4/sm/' + mod_numb + '/' + number + '.gif") 50% no-repeat';
    button.style.cursor = "pointer";
    button.style.marginBottom = "2px";
    button.style.marginRight = "2px";
    button.addEventListener("click", function () {puttext("/SM  *"+number+"*")}, false); 				//dojde k automatickemu prevodu ciselne hodnoty na text
    return button;
}

function create_admin_button(nazev, udalost, tooltip){							// pro vytvoreni admin tlacitka
    var button = this.document.createElement('button');
    var txt = this.document.createTextNode(nazev);
    button.style.position = "relative";
    button.style.font = "7pt Arial";
    button.style.color = "black";
    button.style.width = "39px";
    button.style.height = "18px";
    button.style.border = "1";
    button.style.cursor = "pointer";
    button.style.marginBottom = "2px";
    button.style.marginRight = "2px";
    button.insertBefore(txt, null);
    button.title = tooltip;
    button.addEventListener("click", function () {udalost(false)}, false);
    if (nazev=="LK/UK" || nazev=="NFLTR") button.addEventListener("contextmenu", function () {udalost(true)}, false);
    return button;
}

function puttext(what) {		// vpise text na radek nebo pro refresh radku + i pro vkladani smajliku//pokud nejde o smajl nebo refresh..zmackne tl.odeslat
	var input = top.window.frames[3].document.getElementById("msg");
	var send_button = top.window.frames[3].document.getElementsByName("submit_text").item(0);
	input.value = what.match(/^\/SM /) !=null ? input.value+=what.replace(/^\/SM /g,"") : what;
	if(what !="" && what.match(/^\/SM /)==null) send_button.click();
	input.focus();
}

function stripchars(correct){											//Fce co odstrani nepovolene znaky v nicku, vychazi z pravidel pro registraci nicku!//EDIT: Jelikoz sem obejvil nekolik nicku co maj vice tecek a i na konci musel sem zmenit kod...ale do budoucna necham zalomitkovany kod presne dle reg. pravidel
	correct = correct.replace(/[^a-zA-Z0-9\._-]+/g,"");					//- neobsahuje české znaky - obsahuje pouze znaky a-z, A-Z, 0-9, . (tečka), _ (podtržítko), - (pomlčka)
	correct = correct.replace(/^[\._-]+/g,"");
	return correct
}

function stripmez(text){											//Fce co odstrani mezery z textu
	return text.replace(/ +/g,"");
}

function refreshiframe(){											//Pro refresh (reset) useriframe
	if (misto.src.match(/http:\/\/xchat\.beedoo\.net\/admin\/Support/g)== null){
		misto.height="90px";misto.width="160px";
		misto.src="http://xchat.beedoo.net/admin/Support/x.php?hash="+hash;
	}
}

function zdroj(p){ 				//Zjistuje, jestli je text (nick,nazev, vyraz,..)  brany ze  skla nebo z radku//Lze nastavit prioritu ci prvni ze skla nebo z radku zamenou poradi,// parametr p pokud je roven 0, pak se vypne odstraneni nevhodnych znaku
   	var input = top.window.frames[3].document.getElementById("msg");
   	var nick = "";
   	if(input.value != "") nick = p==0 ? input.value : stripchars(input.value);
   	else if(glass.document.getSelection() != "") nick = p==0 ? glass.document.getSelection() : stripchars(glass.document.getSelection());
   	return nick;
}

function refrURL()top.window.location.href=top.window.location.href;	//Fce pro refresh stranky//prikaz location.reload() delal bordel ..toto funguje lip :)
//**********************************************************************************************************************************

//**************************************************Funkce pro admin tlačítka*******************************************************
function unkickall() puttext('/unkickall');
function klice() window.open("http://xchat.centrum.cz/"+hash+ "/modchat?op=roomframeng&menuaction=adminpageng&rid="+rid,"Klice");
function zapisfiltr() misto.setAttribute("src",'http://xchat.centrum.cz/~guest~/modchat?op=roominfo&rid=' +rid+" /filtr/"+hash+"/"+email);

function dvere(rightclick){
	if (rightclick) puttext("/unlock");  		//prikaz pro radek
	else puttext("/lock");						//prikaz pro radek
}

function stalyspravce(){
	nick = zdroj() == "" ? prompt("Zobrazení záznamu o SS  - Zadej nick!", "") : zdroj();
	if(nick !=null && stripchars(nick) !="") window.open("http://xchat.beedoo.net/stali/stalispravci.php?gs=1&n="+stripchars(nick),"SS - "+nick);
	else alert("Nezadali jste hledaný nick!");
	puttext("");   		//Ciste pro refresh a nastaveni aktivnosti radku pro psani
}

function profil(){
	if(zdroj() != "") window.open("http://xchat.cz/"+zdroj(),"Profil - "+zdroj());
	puttext("");
}

function nastavfiltr(rightclick){
	if (rightclick){
		puttext("/filtr 0");				//rychle zruseni filtru pomoci praveho tl. mysi
		refreshiframe();
	}
	else{
		filtrlength=document.getElementById("delkaf");
		delkafiltru = filtrlength.value == "" ? prompt("Zadej délku filtru (0-750).", "") : filtrlength.value; 	//kdyz bude zvoleno Zadat ..vyskoci okno kde se zada delka filtru...pokud se vybere neco jineho pouzije se definovana delka(prednastavena)
		if(isNaN(delkafiltru)) alert("Příkaz neproveden! Nesprávný formát zadané hodnoty!");  // osetreni pri zadani textu a ne cisla, vyuzivam zvlastni vlastnosti ze isNaN() bere neexistujici hodnotu jako cislo!
		else if(delkafiltru ==null ||stripmez(delkafiltru) =="")alert("Příkaz neproveden! Nezadali jste délku filtru!");  //osetreni pri zmacknuti tl. storno ci nezadani hodnoty ci jen mezer
		else if (delkafiltru <0 || delkafiltru >750) alert("Příkaz neproveden! Hodnota není ze správného intervalu (0-750)!"); //osetreni pro prilis dlouhe filtry ci zaporna cisla
		else {
			delkafiltru=Math.round(delkafiltru);         			//Osetreni na desetina cisla ..zaokrouhli aritmeticky
			puttext("/filtr "+delkafiltru);							//Provedeni vlastniho prikazu
			refreshiframe();
		}
	}
	puttext("");
}
//************************************************Funkce pro multifunkcni tlacitko AKCE*********************************************
function action(){											//mnohofunkcni funkce dle vyhodnoceneho prikazu v radku 
	var prikaz = zdroj(0).match(/^\/\w+|^\/\?/);			//vznika pole a to by nemelo byt string, ale zde zarucena vzdy jen 0 polozka pole takze bude prevedeno spravne pri volani celeho pole a neni treba vsude psat prikaz[0], navic zde se to hodi pri neexistenci prikazu v textu nemusim osetrovat jestli existuje nebo ne pole, protoze se pro replace text(pro hledani) bude rovnat null a to zkousne
	var vyraz = zdroj(0).replace(prikaz+" ","")==zdroj(0)? "" :zdroj(0).replace(prikaz+" ","");  // jde o to, jestli bude jen prikaz bez vyrazu a mezery za prikazem nebo bude prikaz a povinna mezera (pak muze a nemusi byt vyraz)
	if(prikaz == "/?"){
		otop=(screen.availHeight-470)/2;
	   	oleft=screen.width-700;
	   	myWindow=window.open('',"HELP","width=500,height=450,resizable=no,location=no,status=no,top="+otop+",left="+oleft);
		myWindow.document.close();							//aby kdyz nezavru okno a znova zadam /? se mi tam nezdvojoval text ale zavrel a nacetl znova do stejneho okna!
		myWindow.document.write("<h3><center>NÁPOVĚDA PRO TLAČÍTKO \"AKCE\"</center></H3><small>Všechny příkazy a hledané výrazy se píší do řádku psaného textu a pak se klikne na tlačítko \"AKCE\". Hledané výrazy se píší za příkazy a předchází je vždy mezera! Např.: <b>/v Chinaski</b> .<br><br><b>Dostupné příkazy:<br><i><big>/? </big></i></b>- Vyvolá tuto nápovědu.<br><b><i><big>/w </big></i></b>- Slouží pro vyhledávání na Wikipedii.<br><b><i><big>/g </big></i></b>- Slouží pro vyhledávání na Google.<br><b><i><big>/s </big></i></b>- Slouží pro vyhledávání ve slovníku cizích slov.<br><b><i><big>/v </big></i></b>- Slouží pro vyhledávání videí na Youtube (Zatím nefungují české znaky!).<br><b><i><big>/csfd </big></i></b>- Slouží pro vyhledávání filmů, herců, režisérů v Česko-Slovenské filmové databázi.<br><b><i><big>/rn </big></i></b>- Slouží pro vstup do místnosti dle zadaného názvu místnosti.<br><b><i><big>/rid </big></i></b>- Slouží pro vstup do místnosti dle zadaného RID.<br><b><i><big>/rrid </big></i></b>- Slouží pro vstup do místnosti dle zadaného RID (Používá se při chybě, kdy už jsem v místnosti, ale nemohu se do ní dostat!).<br><b><i><big>/nojava </big></i></b>- Slouží pro otevření okna aktuální místnosti s vypnutými Java scripty (Používá se při chybě, kdy se mi nechce načíst sklo nebo je šedé!).<br><b><i><big>/prl </big></i></b>- Slouží pro překlad z AJ do CJ.<br><b><i><big>/radio </big></i></b>- Otevře oblíbené rádio Waper :).</small>");
		myWindow.document.write("<HR><center><FORM><INPUT TYPE='button' VALUE='Zavřít' title='Zavřít' style='cursor:pointer' onClick='window.close()'></FORM></CENTER>");
		myWindow.focus();
	}	
	if(prikaz == "/w"){
		vyraz = stripmez(vyraz) == "" ? prompt("Wikipedie - Zadej hledaný výraz!", "") : vyraz;
		if(vyraz !=null && stripmez(vyraz) !="")window.open("http://cs.wikipedia.org/w/index.php?title=Speci%E1ln%ED%3AHled%E1n%ED&search="+vyraz);
		else alert("Nezadali jste hledaný výraz!");
	}
	else if(prikaz == "/g"){
		vyraz = stripmez(vyraz) == "" ? prompt("Google - Zadej hledaný výraz!", "") : vyraz;
		if(vyraz !=null && stripmez(vyraz) !="")window.open("http://www.google.cz/#hl=cs&q="+vyraz);
		else alert("Nezadali jste hledaný výraz!");
	}
	else if(prikaz == "/s"){
		vyraz = stripmez(vyraz) == "" ? prompt("Slovník cizích slov - Zadej hledaný výraz!", "") : vyraz;
		if(vyraz !=null && stripmez(vyraz) !="")window.open("http://slovnik-cizich-slov.abz.cz/web.php/hledat?typ_hledani=prefix&cizi_slovo="+vyraz);
		else alert("Nezadali jste hledaný výraz!");
	}
	else if(prikaz == "/v"){
		vyraz = stripmez(vyraz) == "" ? prompt("Youtube - Zadej hledané video!", "") : vyraz;
		if(vyraz !=null && stripmez(vyraz) !="")window.open("http://www.youtube.com/results?search_query="+vyraz);
		else alert("Nezadali jste hledané video!");
	}
	else if(prikaz == "/csfd"){
		vyraz = stripmez(vyraz) == "" ? prompt("Česko-Slovenská filmová databáze - Zadej hledaný film,herce,režiséra!", "") : vyraz;
		if(vyraz !=null && stripmez(vyraz) !="")window.open("http://www.csfd.cz/hledani-filmu-hercu-reziseru-ve-filmove-databazi/?search="+vyraz);
		else alert("Nezadali jste hledaný výraz!");
	}
	else if(prikaz == "/rn"){
		vyraz = stripchars(vyraz) == "" ? prompt("Zadej název místnosti!", "") : vyraz;
		if(vyraz !=null && stripchars(vyraz) !="")window.open("http://xchat.centrum.cz/"+hash+"/modchat/room/"+vyraz+"&_btn_enter=true&disclaim=true&sexwarn=1","Room dle nazvu- "+vyraz);
		else alert("Nezadali jste název misntosti!");
	}	
	else if(prikaz == "/rid"){
		vyraz = stripchars(vyraz) == "" ? prompt("Zadej RID!", "") : stripchars(vyraz);
		if(vyraz !=null && stripchars(vyraz) !="")window.open("http://xchat.centrum.cz/"+hash+"/room/intro.php?rid="+stripchars(vyraz)+"&_btn_enter=true&disclaim=true&sexwarn=1","Room dle RID- "+vyraz);
		else alert("Nezadali jste RID!");
	}
	else if(prikaz == "/rrid"){
		vyraz = stripchars(vyraz) == "" ? prompt("Zadej RID!", "") : stripchars(vyraz);
		if(vyraz !=null && stripchars(vyraz) !="")window.open("http://xchat.centrum.cz/"+hash+"/modchat?op=mainframeset&rid="+stripchars(vyraz),"Room - RST"+vyraz);
		else alert("Nezadali jste RID!");
	}
	else if(prikaz == "/nojava")window.open("http://xchat.centrum.cz/"+hash+"/modchat?op=mainframeset&rid="+rid+"&skin=4&js=0&nonjs=1","Room - no Java"+rid);
	else if(prikaz == "/prl"){
		vyraz = stripmez(vyraz) == "" ? prompt("Zadej výraz pro překlad!", "") : vyraz;
		if(vyraz !=null && stripmez(vyraz) !="")window.open("http://translate.google.cz/?hl=cs&tab=wT#en|cs|"+vyraz,"Preklad slova - "+vyraz);
		else alert("Nezadali jste výraz!");
	}
	else if(prikaz == "/rev"){
		revveta="";
		if (vyraz.search(": ")!=-1 && stripchars(vyraz.slice(0,vyraz.search(": ")))== vyraz.slice(0,vyraz.search(": ")) && vyraz.slice(0,2) !=": "){	
			revveta=vyraz.slice(0,vyraz.search(": ")+2);		//ucelem je najit nick kteremu to posilam ..povinna je za nickem : a mezera za ni!
			vyraz = vyraz.replace(revveta,"");					//odstraneni nicku z vyrazu
		}
		vyraz = stripmez(vyraz) == "" ? glass.document.getSelection() : vyraz;
		smajl = smajl2 = new Array();
		pocet=vyraz.length;													//pocet znaku v textu pro revers
		if (stripmez(vyraz)!=""){
			for (a=pocet-1; a>-1; a--)revveta+=vyraz.charAt(a);
			smajl = vyraz.match(/\*\d+\*/g);								//Vyhledani smajlu orig textu do pole
			smajl2 = revveta.match(/\*\d+\*/g);								//Vyhledani smajlu obraceneho textu do pole
			if (smajl !=null) for (i=0; i<smajl.length; i++)revveta=revveta.replace(smajl2[i],smajl[smajl.length-1-i]);
		puttext(revveta);
		}
	}
	else if(prikaz == "/radio")window.open("http://radiowaper.listen2myradio.com/","Radio");
	else if(prikaz == "/gooryk"){
    	if (confirm("Máte rádi gooryka?")) alert("♥♥♥ On vás taky má rád :o* :o* ♥♥♥");
    	else alert("Hmm, budu smutný :(:(:(");
   	}
    puttext("");
}
//***********************************Vytvoreni prvku Refresh v hornim Menu vedle meho nicku.****************************************
top.window.addEventListener("load", function () {hornimenu()}, false);			//ne vzdy existuje misto kam se vklada driv jak tento script----usetri to tak 6radku kodu, se stejnym efektem

function hornimenu(){
	var page=top.window.frames[4];
	var kam=page.document.getElementsByTagName('h2')[0];
	var mynicklabel=page.document.getElementsByTagName('span')[1];					//prvek s mym nickem .. vpravo nahore
	mynicklabel.setAttribute("style","cursor:pointer;border: 1px dotted white;");
	mynicklabel.title="Otevřít můj profil";
	mynicklabel.setAttribute("onclick",'window.open("http://xchat.centrum.cz/'+hash+'/whoiswho/profile.php?nick='+mynicklabel.innerHTML+'","'+mynicklabel.innerHTML+'");top.window.frames[3].document.getElementById("msg").focus()');
	var sirka=this.innerWidth-mynicklabel.offsetWidth-32;							//pro kazdy nick by to bylo jinak, tak sem to vyresil automatickym zjistenim zbyvajici sirky do praveho konce ramu
	kam.innerHTML+="<span class='l'>&nbsp;</span><span id='toprefresh' title='Šetrnější refresh jak F5' style='cursor:pointer;border: 1px dotted white;text-align:center;width:"+sirka+"px'>Refresh</span><span class='r'>&nbsp;</span>";
	var refrtl=page.document.getElementById("toprefresh");
	refrtl.addEventListener("click", function () {refrURL()}, false);
}
//**********************************************************************************************************************************

//**********************************************Sekce osetreni chyb vlivem zpozdeni apod.*******************************************
var nacitani;
if (top.window.document.readyState!="complete") nacitani=window.setInterval(check,100);		//test jestli bylo zmacknuto F5
else proved();													//nacte URL pokud je hlavni okno (stranka) nactena, tudiz doslo k reload userframe jinak nez F5 ci proste refresh cele stranky..

function check(){							//Unikatni funkce co testuje (vyhleda) presnycas zobrazeni (nacteni) skla i s textem
	if (top.window.frames[1].frames[0].document.readyState=="complete"){
		if (top.window.frames[1].frames[0].document.body.innerHTML.match(/<div id="board"><div id="splash">Nahrávám\.\.\. <img src=/g)==null){
			clearInterval(nacitani);
			proved();
		}
	}
}

function proved(){
	glass = top.window.frames[1].frames[0];												//velmi vyjimecne se stavalo, ze nefungovalo oznaceni ze skla a script se kousl, tenhlete cely system zajisti ze uz ani to se nestane
	glass.addEventListener("mouseover", function () {skryjsmajly()}, false);			//osetreni kdy nekdy se stalo ze sklo jeste nebylo a nepriradila se tato funkce!!!-proto zde
	//************Refresh nekdy sedeho skla pri vstupu do room nebo F5******************		
	if (glass.document.getElementById("board").innerHTML.match(/^<div style="display: none;" id="splash">/g)!=null) refrURL();
	//************Nacteni URL do inforamu******************
	if (misto.src==self.window.location.href) misto.src="http://xchat.beedoo.net/admin/Support/x.php?hash="+hash;			//Pro pripad kdy kliknu na zapis room driv nez dojde k nacteni cele stranky ..casta chyba diky zpozdeni zbrzdeni xchatu :(, a taky nechapu proc kdyz mam src="" se vzdy pripradi SRC aktualni stranky (window.location.href) omg
	oroom.src="http://xchat.beedoo.net/admin/Support/sidebar.php?rid="+rid+"&hash="+hash;
}

//*********************jen kosmeticka uprava vylozene jen pro dokonalost*************************************
this.addEventListener("load", function () {oblibeni_divup.style.height = (this.innerHeight-30)+'px';}, false)

//*******Kdyz nebudou existovat tlacitka vedle nicku v Bootom Baru, hodi refresh //podstate diky pouziti novych metod by se uz nemusel ani pouzivat ale kdyby nekdy doslo k nake chybe obrovske...hodi se to
//-dulezite-casto pri vstupu do room nebyla tlacitka - zde je tak nejlepsi misto pro kod********
if (top.window.frames[3].document.getElementById("reasonplace") == null) top.window.frames[3].document.getElementsByName("submit_text").item(0).click();