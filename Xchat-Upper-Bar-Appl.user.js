﻿// ==UserScript==
// @Originalname Xchat-Upper-Bar-Appl.user.js
// @Author gooryk
// @include http://*xchat.centrum.cz/*/modchat?op=titlepage&rid=*
// @exclude http://*xchat.centrum.cz/~guest~/modchat*
// ==/UserScript==

//***************************************GLOBALNI PROMENNE**************************************************************************
var glass="";											//pro ostatni funkce jeste pred load celeho window (viz nize), je inicializace zarucena viz uplne dole
var telo=document.getElementsByTagName('body')[0];		//nacteni tela framesky, zde se vlozi cely kod
var odhlasitadress=telo.getElementsByTagName('strong')[3].innerHTML.match(/\".+?\"/g)[0];

//****ORIGINALNI ziskani dat z kodu hlavni stranky..zarucuje ze bude existovat vzdy a tim konecne odstranim plno problemu - nyni novejsi verze osetreni proti zmenam nyni hleda v celem HTML kodu****					
var data = top.window.document.getElementsByTagName("html")[0].innerHTML;		//nacteni zdroje dat
var hash = data.split("var my_auth='")[1].split("';")[0];
var mynick = data.split("var my_nick='")[1].split("';")[0];

document.body.style.background="black";					//originálni barva pozadi, kdyz nepouzivame muj vzhled!
var barva="yellow";  									//barva pisma tzn. odkazu + nadpisu Smajlici
//****************muj vzhled******************
if (top.window.document.getElementById("mainframe").rows=="28,*") document.body.style.background="black url('http://xteam.jaw.cz/gooryk/admin-scripty/data/upper-background-original.jpg')";
else document.body.style.background="black url('http://xteam.jaw.cz/gooryk/admin-scripty/data/upper-background.jpg')";		//obrázek na pozadi
//***************************************Vytvoreni nekterych odkazu a tlacitek********************************************************
telo.innerHTML="<span id='zobraz'><span style='position:relative;top:-6px;color:"+barva+";font-size:15px;font-weight:bold'>&nbsp;&nbsp; Smajlíci >>> </span><img src='http://xteam.jaw.cz/gooryk/admin-scripty/data/Blue-arrow.png' style='height:30px;width:30px'></span><span id='smajly' style='display:none'></span><span id='odkazy' style='position:absolute;color:"+barva+";font-weight:bold;left:100%;margin-left:-280px'><a href='http://forum.xchat.centrum.cz/"+hash+"/forum/favourite.php' target='fora' title='Moje oblíbená fóra' style='color:"+barva+";'>OFora</a>&nbsp;&nbsp;<a href='http://xchat.centrum.cz/help/index.php?root=119#help_411' target='oboti' title='Povolení osobní boti' style='color:"+barva+";'>Oboti</a>&nbsp;&nbsp;<a href='http://xchat.centrum.cz/help/index.php?root=118#help_412' target='iboti' title='Povolení infoboti' style='color:"+barva+";'>Iboti</a>&nbsp;&nbsp;<a href='http://xhodiny.doni.eu/profil/"+mynick+"' target='xhodiny' title='Moje Xhodiny (pokud tam mám profil)' style='color:"+barva+";'>Xhodiny</a>&nbsp;&nbsp;<a href='http://xteam.jaw.cz/gooryk/Stream/Videostream.html' target='videostream' title='Online vysílání, filmy, klipy,... (na přání).' style='color:"+barva+";'>Vstream</a>&nbsp;&nbsp;<input type='button' style='border: 1px dotted #00FF00;font-style:obligue;font-size:12px;background-color: transparent;font-weight:bold;width:65px;margin-top:4px;cursor:pointer;color:#00FF00' title='Odhlásit' value='Odhlásit' onclick='top.location.href="+odhlasitadress+"'></span>";

var zobrazsm=document.getElementById("zobraz");
var smbutdiv=document.getElementById("smajly");
var odkazy=document.getElementById("odkazy");

zobrazsm.addEventListener("mouseover", function () {zobrazsmajly()}, false);
//****************************************Vložení smajlikovyych tlacitek**************************************************************
//Kdo nechce tolik smajlů, smaze tolik smajl(xxxx); , kolik bude potreba
//Pro zmenu obsahu smajlu staci zmenit pouze cislo napr chci *3* tak prepisu nejaky na smajl(3);

function smajl (cislo) smbutdiv.appendChild(smiley(cislo));	//umisteni smajlikoveho tlacitka

smajl(4387);smajl(1941);smajl(5341);smajl(4442);smajl(1242);smajl(643);smajl(1743);smajl(4244);smajl(4144);smajl(1645);
smajl(1945);smajl(4146);smajl(1446);smajl(1947);smajl(4547);smajl(5047);smajl(349);smajl(2549);smajl(2749);smajl(2849);
smajl(4050);smajl(2850);smajl(252);smajl(2252);smajl(2752);smajl(2852);
//**********************************************************************************************************************************

//********************************************************POMOCNE FUNKCE************************************************************
function searchmyself(){		//Fce co najde a zvyrazni prichod a odchod meho nicku, dobre v nestalych room, bych videl zda a kdy sem tam uz kontroloval
	glass= top.window.frames[1].frames[0];					//nekdy tam proste neco neni a nevyhledava, proto tady kdyby nahodou se poprve nepodarilo inicializovat
	sklo=glass.document.getElementById("board");
	radky=sklo.getElementsByTagName("div");
	var vstup=new RegExp("Uživatel.*? <b.*?>"+mynick+"<\/b> vstoupil.*? do místnosti","gi");
	var exit=new RegExp("Uživatel.*? <b.*?>"+mynick+"<\/b> opustil.*? místnost|Uživatel.*? <b.*?>"+mynick+"<\/b> byl.*? vyhozen.*? ","gi");
	for (a=0;a<radky.length;a++){
		if (radky[a].innerHTML.match(vstup)!=null && radky[a].getElementsByTagName("font")[1]!=undefined) radky[a].getElementsByTagName("font")[1].setAttribute("style","color:green;background-color:#E5FFCB");
		else if (radky[a].innerHTML.match(exit)!=null && radky[a].getElementsByTagName("font")[1]!=undefined) radky[a].getElementsByTagName("font")[1].setAttribute("style","color:red;background-color:#E5FFCB");
	}
}

function zobrazsmajly(){
	glass.addEventListener("mouseover", function () {skryjsmajly()}, false);	//osetreni kdy nekdy se stalo ze sklo jeste nebylo a nepriradila se tato funkce!!!-proto zde//tady se to sice nestavalo ale je to dokonalejsi
	smbutdiv.style.display='inline-block';
	zobrazsm.style.display='none';
	odkazy.style.display='none';
}

function skryjsmajly(){
	smbutdiv.style.display='none';
	zobrazsm.style.display='inline-block';
	odkazy.style.display='inline';
}

function puttext(what) {		// vpise text na radek nebo pro refresh radku + i pro vkladani smajliku//pokud nejde o smajl nebo refresh..zmackne tl.odeslat
	var input = top.window.frames[3].document.getElementById("msg");
	var send_button = top.window.frames[3].document.getElementsByName("submit_text").item(0);
	input.value = what.match(/^\/SM /) !=null ? input.value+=what.replace(/^\/SM /g,"") : what;
	if(what !="" && what.match(/^\/SM /)==null) send_button.click();
	input.focus();
}

function smiley(cislo){  										// pro vytvoreni smajlikoveho tlacitka
	var button = document.createElement('button');
	var mod_numb = cislo % 100;
	button.style.width = "42px";
	button.style.height = "30px";
	button.style.border = "0";
	button.style.background = 'url("http://img.xchat.centrum.cz/images/x4/sm/' + mod_numb + '/' + cislo + '.gif") 50% no-repeat';
	button.style.cursor = "pointer";
	button.addEventListener("click", function () {puttext("/SM  *"+cislo+"*")}, false);
	return button;
}
//**********************************************************************************************************************************

//****************************************************Hlavni program****************************************************************
//**********************************************Ochrana proti chybam vlivem refresh a zpozdeni**************************************
var nacitani;
if (top.window.document.readyState!="complete") nacitani=window.setInterval(check,100);		//test jestli bylo zmacknuto F5
else searchmyself();																		//pro ultra okamzite nacteni pro pripady kdy nejde o F5

function check(){							//Unikatni funkce co testuje (vyhleda) presnycas zobrazeni (nacteni) skla i s textem
	if (top.window.frames[1].frames[0].document.readyState=="complete"){
		if (top.window.frames[1].frames[0].document.body.innerHTML.match(/<div id="board"><div id="splash">Nahrávám\.\.\. <img src=/g)==null){
			clearInterval(nacitani);
			searchmyself();
		}
	}
}
