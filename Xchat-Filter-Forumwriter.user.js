﻿// ==UserScript==
// @Originalname Xchat-Delroom-Filter-Forumwriter.user.js
// @Author gooryk
// @include http://*xchat.centrum.cz/~guest~/modchat?op=roominfo&rid=*
// ==/UserScript==

//******************************************************GLOBALNI PROMENNE************** **********************************************
var udaje = location.href.split("/");					//pole udaju ziskany z adresy (hash, email, akce)
var hash = udaje[6];									//promenna hash
var email = udaje[7];									//promenna email
var Akce = udaje[5];									//promenna akce, dle ni se pak vetvi program a dynamicky tvori obsah iframe
var uzivatele = udaje[8];								//Pritomni uzivatele
var zdroj = document.body.getElementsByTagName('td');	//pole bunek puvodni tabulky puvodni stranky
var telo = document.getElementsByTagName('body')[0];	//telo HTML kodu
var datapole = smajladress = new Array;					//promenne typu pole pro smajly a data pro zapis
//************************************************************************************************************************************

//******************************************************Dynamicke vytvoreni stranky***************************************************
for (i=0; i<zdroj.length; i++)datapole.push(zdroj[i].innerHTML);				//vytvori se pole, ktere obsahuje data ze vsech bunek puvodni tabulky na strance..jde o data info o mistnosti
var filtrnum= datapole[19].match(/\d+/g)==null ? 0 : datapole[19].match(/\d+/g)[0];			//vyhleda udaj o filtru a vybere z toho jen cislo, pokud neni nastaveno priradi 0
var filtrtxt= filtrnum==0 ? "NENÍ!!!" : filtrnum+" min";						//vytvoreni textove promenne hodnoty filtru + prida za ni "min", pokud neni nastaveno..prideli hodnotu "NENI!!!"
//*******************vlastni tvorba stranky dle Akce*****************************
if (Akce=="mistnost") telo.innerHTML = "<center><strong><label id='forum' title='Otevřít fórum.' style='cursor:pointer' for 'x'>Zápis zrušené místnosti.</label></strong></center>"+"<p align='left'><b><label id='prepinac' style='cursor:pointer' title='Kliknutí sem aktivuje výběr důvodů.' for 'x'>&nbsp;Zvolte nadpis (důvod):</label></b></p>"+"<select id='reason' title='Výběr důvodů, nadpisů, názvů příspěvků.' style='font-weight:bold;border:solid blue ;color:black;background-color: #BBDDF7;width:155px'><option value='Sex za peníze'>SzP</option><option value='Animal sex'>Animal</option><option value='Incest'>Incest</option><option value='Pedofilie'>Pedofilie</option><option value='Pod 18 let'>  &lt 18</option><option value='Ubytování'>Ubytování</option><option value='Práce'>Práce</option><option value='Brigáda'>Brigáda</option><option value='Vydržování'>Vydržování</option><option value='Telefonní číslo'>Telefonní číslo</option><option value='Kredit'>Kredit</option><option value='Foto < 18'>Foto &lt 18</option><option value='Komerce'>Komerce</option><option value='Inzerce'>Inzerce</option><option value=''>Zadej vlastní důvod</option></select>"+"<input type='text' id='nadpis' title='Sem vkládejte vlastní znění názvů příspěvků (důvodů,nadpis).' style='font-weight:bold;border:solid blue ;color:black;background-color: #BBDDF7;width:155;display:none' value='Vlož nadpis (důvod)' onclick =\"if(value=='Vlož nadpis (důvod)') value=''\"/><br>"+"<table title='Tabulka hodnot, které se zapíší na fórum (info o místnosti).' style='table-layout:fixed' cellspacing='0' id='Tab1' border='0'><tr><th align='left' width='50px'>"+datapole[1]+"</th><td align='left' width='105px'>"+datapole[2]+"</td></tr><tr><th align='left'>"+datapole[3]+"</th><td align='left'>"+datapole[4]+"</td></tr><tr><th align='left'>"+datapole[5]+"</th><td align='left'>"+datapole[6]+"</td></tr><tr><th align='left'>"+datapole[7]+"</th><td align='left'>"+datapole[8]+"</td></tr><tr><th align='left'>"+datapole[9]+"</th><td align='left'>"+datapole[10]+"</td></tr></table><br>"+"<p title='Zatržením se na konec vloží výpis všech zúčastněných v místnosti.' align='left'>&nbsp;<input type='checkbox' id='uzivcheck'><b> připsat nicky v místnosti.</b></p><br>"+"<p align='left'><b>&nbsp;Důkazy apod.:</b></p>"+"<small><textarea id='duvody' title='Sem vkládejte důkazy, nicky zakladatelů a jiné info.' style='font-weight:bold;border:solid orange ;color:blue;background-color: #F4DEC2;width:155px;height:150'></textarea><small><small><br><br></small></small>"+"<input type='button' id='OK' style='cursor:pointer' title='Zapsat na fórum.' value='Zapsat' />"+"<iframe style='position:absolute;visibility:hidden;width:0px;height:0px;left:0px;top:99%' id='delroomwriter' src=''></small></iframe>"
if (Akce=="filtr") telo.innerHTML = "<center><strong><label id='forum' title='Otevřít fórum.' style='cursor:pointer' for 'x'>Zápis časového filtru.</label></strong></center>"+"<small><p align='left' title='Tato hodnota filtru + zvolený nebo zadaný důvod budou zapsány na fórum.' >&nbsp;"+filtrtxt+"; + <i>zadejte důvod...</i><br></p>"+"<select id='reason' title='Zvolte důvod filtru.' style='font-weight:bold;border:solid blue ;color:black;background-color: #BBDDF7;width:155px'><option value='Wilda má zase vycházky.'>Wilda</option><option value='Tapetář, zakládá stále nové nicky.'>Tapetace</option><option value='Prudič, ruší chod místnosti s novými nicky.'>Prudič</option><option value=''>Zadej vlastní důvod</option></select>"+"<input type='text' id='nadpis' title='Sem vložte vlastní znění důvodu pro filtr. Dvojklik sem přepne opět na výběr důvodů.' style='font-weight:bold;border:solid blue ;color:black;background-color: #BBDDF7;width:155;display:none' value='Vlož vlastní důvod.' onclick =\"if(value=='Vlož vlastní důvod.') value=''\"/><small><small><br><br></small></small>"+"<input type='button' id='OK' style='cursor:pointer' title='Zapsat na fórum.' value='Zapsat' />"+"<iframe style='visibility:hidden;width:0px;height:0px' id='delroomwriter' src=''></iframe>"+"<input type='button' id='storno' title='Zrušit akci, návrat k původním info oknům.' value='Zrušit' style='cursor:pointer' onclick=\"window.location.href='http://xchat.beedoo.net/admin/Support/x.php?hash="+hash+"'\"/></small>";
//************************************************************************************************************************************

//************Opet nekompatibilita musi se prevest adresne smajly na tvar*???* + seskladani INFO o mistnosti dohromady****************
smajladress = datapole[6].match(/<img src="http:\/\/img\.xchat\.centrum\.cz\/images\/x4\/sm\/.+?\*">/g);    //k vyhledani smajlu v podobe odkazu//prepinac g je jasny ..zatracena nenasytnost
if (smajladress !=null)for (i=0; i<smajladress.length;i++)datapole[6]=datapole[6].replace(smajladress[i],smajladress[i].match(/\*\d+\*/g)[0]);
var data="<b>"+datapole[1]+"</b>%20"+datapole[2]+"%0a<b>"+datapole[3]+"</b>%20"+datapole[4]+"%0a<b>"+datapole[5]+"</b>%20"+datapole[6]+"%0a<b>"+datapole[7]+"</b>%20"+datapole[8]+"%0a<b>"+datapole[9]+"</b>%20"+datapole[10]+"%0a";			//%0a je v URL kodovano zalomeni radku ....%20...je v URL mezera
data=data.replace(/&amp;/g,"");					//odstraneni znaku co mi nicil zapisky ..pac si to myslelo ze jde o zadani promenne!!
//************************************************************************************************************************************

//*************************Prirazeni funkci pro zvolene udalosti zvolenych prvku a jeste dle Akce*************************************
var	duvodboxTXT=document.getElementById("nadpis");			//objekt textove pole pro nadpis (mistnost) resp. duvod (filtr) cili (obe akce)
if (Akce == "filtr") duvodboxTXT.addEventListener("dblclick", function () {AkcePrepinac()}, false); //jako prepinac na vyber. boxik duvodu pri dvojkliku - pouze pro (filtr) .. prepnuti zajisti odkaz. funkce
var duvodboxvyber=document.getElementById("reason");			//objekt vyberovy boxik duvodu (pro obe akce)
	duvodboxvyber.addEventListener("click", function () {Akceduvodboxvyber()}, false); //pri kliku na hodnote "Zadej vl. duvod" odkazov. funkce prepne na textove pole pro zadani vl. duvodu (obe akce)
if (Akce == "mistnost"){									//pouze pro akci mistnost...
	var uzivcheck=document.getElementById('uzivcheck');		//check box pro zvoleni zapisu uzivatelu
	var duvodarea=document.getElementById("duvody");		//textarea duvodu...dukazu atd
	var prepinatko=document.getElementById("prepinac");		//objekt label
		prepinatko.addEventListener("click", function () {AkcePrepinac()}, false); //prepinac pri kliku na nej (label) prepne odkaz. funkce zpet na vyber. boxik duvodu
}		
document.getElementById("OK").addEventListener("click", function () {Start()}, false);	//pro obe akce ..klik na tl. Zapsat provede odkazovanou fci
document.getElementById("forum").addEventListener("click", function () {openforum()}, false);
//************************************************************************************************************************************

//********************************************************Pomocné funkce**************************************************************
function prevodURL(text){						//fce - kvuli nake nekompatibilite v prenaseni v URL je treba nahradit urcite znaky za hexadecimalni kody
	text=text.replace(/ /g,"%20");text=text.replace(/\n/g,"%0a");
	return text;	
}

function backURL()window.location.href="http://xchat.beedoo.net/admin/Support/x.php?hash="+hash;	//refresh adresy na puvodni

function zapisopen(adresa,oknoad,oknonam){						//fce zapisu na fora + otevreni fora
	var deletor = document.getElementById("delroomwriter"); 	//nastroj, co odesle data na forum (iframe)
	deletor.setAttribute("src", adresa);						//ZAPSANI NA FORA
	if (confirm("Zapsáno! - Otevřít fórum pro kontrolu?")) window.open("http://xchat.centrum.cz/"+hash+oknoad,oknonam);
}
//************************************************************************************************************************************

//**********************************************Vlastni funkce tlacitek a prvku*******************************************************
function Akceduvodboxvyber(){					//fce, co skryje boxik duvodu a zobrazi text.pole pro vlastni duvody
	if (duvodboxvyber.value==''){
		duvodboxvyber.style.display='none';
		duvodboxvyber.selectedIndex=0;
		if (Akce == "mistnost") prepinatko.innerHTML="&nbsp;Návrat k volbám klikni sem:";
		duvodboxTXT.style.display='inline-block';
		duvodboxTXT.focus();
	}
}

function AkcePrepinac(){						//fce, co skryje text. pole vlastnich duvodu(nazvu,nadpisu) a prepne na boxik s vyberem duvodu
	if (Akce == "mistnost") prepinatko.innerHTML="&nbsp;Zvolte nadpis (důvod):";
		duvodboxTXT.style.display='none';
		duvodboxvyber.style.display='inline-block';
}

function openforum(){
	if (Akce == "mistnost"){var oknoad = "/forum/topic.php?root=8&id_parent=8&id=3040"; var	oknonam = "Zrusene mistnosti";}
	if (Akce == "filtr"){var oknoad = "/forum/topic.php?root=4&id_parent=4&id=3363"; var oknonam = "FiltryZapis";}
	window.open("http://xchat.centrum.cz/"+hash+oknoad,oknonam);
}


function Start(){								//HLAVNI FUNKCE PRO ZAPIS na fora (fce tl. Zapsat)
	var nadpis = duvodboxvyber.style.display=='none' ? duvodboxTXT.value : duvodboxvyber.value;			//Vytvoreni promenne pro nadpis(mistnost) nebo duvod filtru (filtr)
	if (Akce=="mistnost"){
		var oknoad = "/forum/topic.php?root=8&id_parent=8&id=3040"; var	oknonam = "Zrusene mistnosti";
		var duvody = uzivcheck.checked==true ? duvodarea.value+"%0a%0a<b>účastníci: </b>"+uzivatele : duvodarea.value;			//objekt textarea kam se pisi duvody pro del room, pokud je zatrzen check box uzivatelu pridaji se nakonec jeste uzivatele
		duvody = duvody.replace(/&/g,"");					//odstraneni znaku co mi nicil zapisky ..pac si to myslelo ze jde o zadani promenne!!Zajimave je ze tady uz to nesmi byt &amp; ale jde o cisty text cili jen hledat &  ..nejspise v datech jde o html znak
		if (nadpis=="Vlož nadpis (důvod)" || nadpis=="" ) alert("Chyba! Nevložili jste nadpis příspěvku!");
		else {
			var adresa = "http://forum.xchat.centrum.cz/"+hash+"/forum/topic.php?root=8&id_parent=8&id=3040&user_mail="+email+"&topic_title="+prevodURL(nadpis)+"&content="+prevodURL(data)+"%0a"+prevodURL(duvody)+"&id_topic=0&sent=1"; 		//vytvoreni adresy pro nastroj..zde pro mistnost ..ze vsech tech ziskanych promennych
			zapisopen(adresa,oknoad,oknonam);
			telo.innerHTML = "<br><br><br><marquee><div style='font-size:60px;color:red;'><br><br>ZAPSÁNO, ZRUŠTE MÍSTNOST!</div></marquee>";
		}
	}
	if (Akce=="filtr"){
		var nazevroom = datapole[2];
		var oknoad = "/forum/topic.php?root=4&id_parent=4&id=3363"; var	oknonam = "FiltryZapis";
		if (nadpis=="Vlož vlastní důvod." || nadpis=="") alert ("Chyba! Nevložili jste důvod pro filtr!");
		else if (filtrnum==0){
			if (confirm("Chyba! Nelze zapsat nulový filtr!\nChcete zapsat sundání filtru?")){
				var adresa = "http://forum.xchat.centrum.cz/"+hash+"/forum/topic.php?root=4&id_parent=4&id=3363&user_mail="+email+"&topic_title="+nazevroom+"&content=Sundáno.&id_topic=0&sent=1";		//vytvoreni adresy pro sundání filtru
				zapisopen(adresa,oknoad,oknonam);
			}
			backURL();												//navrat na puvodni adresu
		}
		else{
			var adresa = "http://forum.xchat.centrum.cz/"+hash+"/forum/topic.php?root=4&id_parent=4&id=3363&user_mail="+email+"&topic_title="+nazevroom+"&content="+filtrtxt+"; "+nadpis+"&id_topic=0&sent=1";		//vytvoreni adresy pro nastroj..zde pro filtr ..ze vsech tech ziskanych promennych
			zapisopen(adresa,oknoad,oknonam);
			backURL();
		}
	}
}